/*
	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
    2. Create a class constructor able to receive 3 arguments
        It should be able to receive 3 strings and a number
        Using the this keyword assign properties:
        username, 
        role, 
        guildName,
        level 
  assign the parameters as values to each property.
  Create 2 new objects using our class constructor.
  This constructor should be able to create Character objects.


	Pushing Instructions:

	Create a git repository named S24.
	Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	Add the link in Boodle.

*/

//Solution: 

/*Debug*/
let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101","Social Sciences 201"]
}


let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}

console.log(student1);
console.log(student2);
/*Debug and Update*/
// Arrow function

const introduce = (student) => {
	console.log(`Hi! I'm ${student.name}. I am  ${student.age} years old.`);
	console.log(`I study the following courses: ${student.classes}`);
}

// function introduce(student){

// 	console.log("Hi! I'm " + student.names + "." + "I am " + student.ages + " years old.")
// 	console.log("I study the following courses: " + student.class)

// }

introduce(student1);
introduce(student2);
// Arrow function

const  getCube = (num) => num **3;
let cube = getCube(3);
console.log(cube);

// function getCube(num){

// 	console.log(Math.pow(num,3));

// }


console.log("Display forEach function: ");

let numArr = [15,16,32,21,21,2];

numArr.forEach ((num) => {
	console.log(num);
});


const numsSquared = numArr.map((num) => num ** 2);


// let numsSquared = numArr.map((num) => num** 2);

// 	return number ** 2;

//   }
// )

console.log(numsSquared);


// 2. Create a class constructor able to receive 3 arguments
//         It should be able to receive 3 strings and a number
//         Using the this keyword assign properties:
//         username, 
//         role, 
//         guildName,
//         level 
//   assign the parameters as values to each property.
//   Create 2 new objects using our class constructor.

 class Character{
 	constructor(username,role,guildName,level){
 		this.username = username;
 		this.role = role;
 		this.guildName = guildName;
 		this.level = level;
 	}
 }

 let Character1 = new Character("Bebe","Mage","My Boss","100");
 let Character2 = new Character("Molly","Fighter","My Boss","101");

 console.log(Character1);
 console.log(Character2);